[TOC]

#### String常用方法

- length():或取字符的长度,也就是字符的个数

- trim():去除字符串首尾的空白字符

- toUpperCase()/toLowerCase():将字母全部转为是大写/小写

- startsWith(String a)/endsWith(String a):以什么开头或以什么结尾

- charAt(index i):或取某个位置的字符

- indexOf(String s)/lastindexOf(String s):获取指定字符的位置

- subString(int start ,int end):从某个位置截取字符串,包含头不包含尾,如果只写一个默认是从当前位置到最后

- 静态方法ValueOf(数据类型 a):将某个类型的数据转为字符串,结果是字符串类型

    ```java
    public class valueOf {
        public static void main(String[] args) {
            int a = 1234;
            String string = String.valueOf(a);
            
        }
    ```

#### StringBuilder

- String是不变对象,每次修改都会创建新的对象,因此String不适合频繁修改操作.

- StringBuilder是一个专门用与修改字符串的一个类,内部维护一个可变的字符数组,操作建立在数组之上进行,

- StringBuilder的三种创建方法

    - 直接创建空串

        ```java
        StringBuilder stringBuilder = new StringBuilder();
        ```

    - 基于字符串创建

        ```java
        String str = "asdfghjkl";
        StringBuilder stringBuilder = new StringBuilder(str);
        // .toString是将builder类型转为字符串
        ```

    - 

#### StringBuilder常用方法

使用时需要new对象,在调用一下方法时`对象名.对应的方法`

- append(String s)------追加(拼接)内容
- delete(index i,index i1)--------删除部分内容
- replace(index i,index i1,String s)------替换部分内容
- insert(index i,String s)--------插入内容

#### 正则表达式

- 用于描述字符串的内容格式，使用它通常匹配一个字符串是否符合格式要求。
- 正则表达式的语法：
    - []:表示一个字符,该字符可以是[]中指定的内容.[0-9]是0-9之间的任意一个数字如果有`^`表示出这几个字符外
    - 预定字符:
        - `.`:任意字符
        - `\d`任意一个数字
        - `\w`:表示任意一个单词即[a-zA-Z0-9_]
        - `\s`:任意一个空白字符
        - `\W`不是单词
        - `\D`:不是数字
        - `\S`:不是空白字符
    - 量词
        - `?`:前面的内容出现0或1次
        -  `+`:最少出现1次
        - `*`:可以出现任意次
        - `{n}`:表示前面的内容出现n次
        - `{n,m}`:表示 前面的内容出现最少n次最多m次
        - `{n,}`:最少出现n次,包括n
    - ():用于分组,是将小括号里的内容看做一个整体
    - 特殊字符需要使用`\`进行转义,eg:`.`需写成`\.`

#### String支持的正则表达式相关方法

- matches():判断字符串是否满足正则表达式
- replaceAll():替换满足正则的部分替换为目标字符
- split():将当前字符按照满足的正则表达式的部分进行拆分
    - 最开始就是可拆项则数字第一个元素为空串
    - 如果中间连续的也会生成一个空串
    - 末尾的直接不要(省略)

#### Object

- 是所有类的父类,所有类都是直接或间接的继承Object,为了多态

- Object中有两个经常被派生类重写的方法:toString()和equals();

    - Object调用toString时会默认返回:类的全名@地址,通常重写返回具体的属性值.

        > 注意String,StringBuilder等方法重写了toString类返回字符串内容

    - Object调用equals时,默认比较的还是`==(比较地址)`常常需要重写euqals()来比较具体的属性值

        > 注意:
        >
        > 1. String类已经重写了equals()来比较字符串内容了,但是StringBuilder并没有
        > 2. 重写equals基本原则:
        >     - 原则上要比较两个对象的属性值是否相同
        >     -  两个对象必须为同一类型,若类型不同则返回false

#### 包装类

- java定义了八个包装类,目的就是为了解决基本类型不能直接参与面向对象开发的问题,是基本数据类型可以通过包装类的形式存在
- 包括Integer,Character,Byte,Short,Long,Float,Double,Boolean,其中Character和Boolean都是继承于Object,其余的6个都继承于java.lang.Number类
- jdk1.5推出了新特性
    - 自动拆装箱,当编译器编译时若发现是基本数据类型之间的相互赋值,则自动补齐代码王城转换工作,这个过程叫自动拆装箱.
- 常用操作
    - 获取基本数据类型的取值范围
    - 可以将字符串转为基本的基本类型