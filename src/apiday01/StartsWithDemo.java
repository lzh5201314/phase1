package apiday01;
/*
 * @ClassName: StartsWithDemo
 * @Data: 2023/10/4 18:41
 * @Author: 西风残叶
 */

public class StartsWithDemo {
    public static void main(String[] args) {
        String str = "asdfghjkl";
        System.out.println(str.startsWith("a"));
    }
}
