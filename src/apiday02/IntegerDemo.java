package apiday02;
/*
 * @ClassName: IntegerDemo
 * @Data: 2023/10/8 14:41
 * @Author: 西风残叶
 */

/**
 * Integer演示
 */
public class IntegerDemo {
    public static void main(String[] args) {
        Integer i1 = new Integer(5000);
        Integer i2 = new Integer(5000);
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));
        Integer i3 = Integer.valueOf(50);
        Integer i4 = Integer.valueOf(50);
        // Integer.valueOf会复用-128-127范围内的数据
        System.out.println(i3 == i4);
        System.out.println(i3.equals(i4));
    }
}
