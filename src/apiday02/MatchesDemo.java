package apiday02;
/*
 * @ClassName: matchesDemo
 * @Data: 2023/10/5 19:19
 * @Author: 西风残叶
 */
/*正则表达式*/

public class MatchesDemo {
    public static void main(String[] args) {
        /*
        * [a-zA-Z0-9_]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
        * \.是正则里面的转义符
        * \\.中的第一个杠十转义正则表达式中\
        * */
        String str = "wangkj@tedu.cn";
        String regex="[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
        boolean match = str.matches(regex);
        if (match){
            System.out.println("是正确的邮箱格式!");
        }else {
            System.out.println("不是正确的邮箱格式!");
        }
    }
}
