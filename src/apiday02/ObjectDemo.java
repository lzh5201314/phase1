package apiday02;
/*
 * @ClassName: ObjectDemo
 * @Data: 2023/10/8 13:45
 * @Author: 西风残叶
 */

public class ObjectDemo {
    public static void main(String[] args) {
        Point p =new Point(100,200);
        System.out.println(p);
    //   和下面的是一样的
        System.out.println(p.toString());
    }
}
