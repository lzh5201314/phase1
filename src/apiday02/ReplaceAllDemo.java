package apiday02;
/*
 * @ClassName: replaceAllDemo
 * @Data: 2023/10/5 19:37
 * @Author: 西风残叶
 */
/*
*
*
* */

public class ReplaceAllDemo {
    public static void main(String[] args) {
        String line = "abc123def456gh78";
        line = line.replaceAll("[0-9]+","#asdfghjkl");
        System.out.println(line);


        String str = "asd  ads";
        str  = str.replaceAll("  ","");
        System.out.println(str);
    }
}
