package apiday02;
/*
 * @ClassName: SplitDemo
 * @Data: 2023/10/5 19:45
 * @Author: 西风残叶
 */

import java.util.Arrays;

public class SplitDemo {
    public static void main(String[] args) {
         String line = "abc123def456gh78";
         String[] date=line.split("[0-9]+");
         String str="";
        for (int i = 0; i < date.length; i++) {
            str+=date[i];
        }
        System.out.println(str);
        System.out.println(Arrays.toString(date));
    }
}