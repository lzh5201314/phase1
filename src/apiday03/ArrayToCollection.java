package apiday03;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Name: ArrayToCollection
 * @Author: 西风残叶
 * @Data: 2023/10/13 18:54
 */
public class ArrayToCollection {
    /**
     * 数组转集合
     */
    public static void main(String[] args) {
        String[] array = {"one", "tow", "three", "four", "five"};
        System.out.println("Array:"+Arrays.toString(array));
        // 接收集合
        Collection c = Arrays.asList(array);
        // 打印集合
        System.out.println("List:"+c);


        array[1] = "six";
        System.out.println("Array:"+Arrays.toString(array));
        System.out.println("List:"+c);
        /**
         * 不能对集合进行增删操作,因为数组是定长的,对集合进行增删操作相当于往数组中加元素/减元素
         */
    }
}
