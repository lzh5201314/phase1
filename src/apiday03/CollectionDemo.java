package apiday03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * @Name: CollectionDemo
 * @Author: 西风残叶
 * @Data: 2023/10/12 22:19
 */
public class CollectionDemo {
    public static void main(String[] args) {

        Collection c = new ArrayList();
        c.add(new Point(1, 2));
        c.add(new Point(3, 4));
        c.add(new Point(5, 6));
        c.add(new Point(7, 8));
        c.add(new Point(9, 0));
        c.add(new Point(1, 2));
        System.out.println(c);
        Point p = new Point(1, 2);
        /**
         * 判断是否包含p对象
         */
        boolean contains = c.contains(p);
        System.out.println(contains);
        // 删除p元素(删除与给定元素equals比较为true的元素)
        boolean remove = c.remove(p);
        System.out.println(remove);
        System.out.println(c);
        // 将pp对象添加到集合中
        Collection cc = new ArrayList();
        Point pp = new Point(1, 2);
        cc.add(pp);
        System.out.println(pp);
        System.out.println(cc);
        // 修改pp中X的值
        pp.setX(100);
        System.out.println(pp);
        System.out.println(cc);
        // // 向上造型
        // Collection c = new ArrayList();
        // // 向字符串中添加字符串
        // c.add("你好");
        // c.add("谢谢");
        // c.add("你好");
        // c.add("请多关照");
        // System.out.println(c);
        // System.out.println(c.size());

        /*
        // 向上造型
        Collection c = new HashSet();// 不允许重复
        // 向字符串中添加字符串
        c.add("你好");
        c.add("谢谢");
        c.add("你好");
        c.add("请多关照");
        System.out.println(c); // 输出集合
        System.out.println(c.size());// 输出元素的个数
        System.out.println("是否为空集:"+c.isEmpty());// 判断是否空集,集合中是否有元素
        c.clear();// 清空集合
        System.out.println("是否为空集:"+c.isEmpty());// 判断是否空集
        */
    }
}
