package apiday03;

/**
 * @Name: CollectionOperDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 15:03
 */

import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合间操作
 */
public class CollectionOperDemo {
    public static void main(String[] args) {
        Collection c1 = new ArrayList<>();
        c1.add("java");
        c1.add("C#");
        c1.add(".NET");
        System.out.println("c1:" + c1);
        Collection c2 = new ArrayList();
        c2.add("android");
        c2.add("ios");
          c2.add("java");
        System.out.println("c2:" + c2);
        // 将c2加到c1中
        c1.addAll(c2);
        System.out.println("c1:" + c1);
        System.out.println("c2:" + c2);

        //  是否包含所有
        Collection c3 = new ArrayList();
        c3.add("C#");
        c3.add("android");
        c3.add("PHP");
        boolean contains = c3.containsAll(c1);
        System.out.println(contains);
        // 取交集,c1中仅保留与c3相同的元素,c3不要
        c1.retainAll(c3);
        System.out.println(c1);
        System.out.println(c3);
        // 删除交集,将c1中与c3相同的元素删除,c3保留
        c1.removeAll(c3);
        System.out.println(c1);
        System.out.println(c3);
    }
}
