package apiday03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @Name: CollectionToArray
 * @Author: 西风残叶
 * @Data: 2023/10/13 18:37
 */
public class CollectionToArray {
    /**
     * 集合转为数组
     */
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("tow");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
        // 将集合转为数组
        /**
         * 若参数数组元素等于集合元素个数,就正常转换
         * 若参数数组元素小于集合元素合数,正常转换(按照集合大小给数组)
         * 若参数数组元素大于集合元素合数,正常转换,同时末尾默认值
         */
        String[] array = c.toArray(new String[0]) ;
        System.out.println(Arrays.toString(array));
    }
}
