package apiday03;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Name: GenericDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 17:13
 */

public class GenericDemo {
    /**
     * 泛型的使用
     */
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("你好");
    }
}
