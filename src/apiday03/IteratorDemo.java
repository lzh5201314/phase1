package apiday03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @Name: IteratorDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 16:19
 */
public class IteratorDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add("one");
        c.add("#");
        c.add("tow");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        c.add("#");
        System.out.println(c);
        // 获取集合c的迭代器
        Iterator it = c.iterator();
        // 询问有没有元素
        // boolean b = it.hasNext();
        while (it.hasNext()) {
            // 取出元素
            System.out.println((String) it.next());
            //  删除#号
            if ("#".equals(it.next())) {
                // c.remove(it.next());// 迭代器无法通过集合的方法来增删元素的,否则会报错.应该使用迭代器的方法
                it.remove();
            }

        }

    }
}
