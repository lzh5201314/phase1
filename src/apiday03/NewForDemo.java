package apiday03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * @Name: NewForDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 16:50
 */
public class NewForDemo {
    public static void main(String[] args) {
        System.out.println("数组基础遍历");
        // 数组遍历
        String[] array = {"one", "tow", "three", "four", "five"};
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("集合基础遍历");
        // 集合遍历
        Collection c = new ArrayList();
        c.add("one");
        c.add("tow");
        c.add("three");
        c.add("four");
        c.add("five");
        Iterator it = c.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // 新循环遍历数组
        System.out.println("数组新循环遍历");
        for (String s : array) {
            System.out.println(s);
        }
        // 新循环遍历集合
        System.out.println("集合新循环遍历");
        for (Object o : c) {
            String str = (String) o; // 建议将其强制转换
            System.out.println(str);
        }
    }
}
