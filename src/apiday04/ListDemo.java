package apiday04;

import java.util.ArrayList;
import java.util.List;

/**
 * @Name: ListDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 20:01
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("tow");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println("list:"+list);
        /**
         * E get(int index)
         * 获取指定元素的下标
         */
        String e = list.get(3);
        System.out.println(e); // three
        System.out.println("----------------------------------------");
        /**
         * E set(int index,E e):
         * 将给定元素(e)设置带给定位置(index),返回原始元素
         */
        String old = list.set(2,"six");
        System.out.println("原来的元素:"+old);
        System.out.println("替换后的集合:"+list);

        /**
         * void add(int index,E e)
         * 将数据插入到指定位置,相当于插入操作
         */
        list.add(3,"你好");
        System.out.println("list:"+list);
        /**
         * E remove(int index):
         * 删除元素并返回指定位置的元素
         */
        String o = list.remove(2);
        System.out.println(o);
        System.out.println("list:"+list);

    }
}
