package apiday04;

import java.util.ArrayList;
import java.util.List;

/**
 * @Name: Lis他Demo2
 * @Author: 西风残叶
 * @Data: 2023/10/13 20:49
 */
public class ListDemo2 {
    /**
     * List提供了获取子集的操作
     * List subList(int start , int end):含头不含尾
     */
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <10; i++) {
            list.add(i*10);
        }
        System.out.println("list:"+list);

        List<Integer> integers = list.subList(2, 7);
        System.out.println("list的子集:"+integers);

        //将子集中的每一个元素都扩大十倍
        for (int i = 0; i < integers.size(); i++) {
            integers.set(i,integers.get(i)*10);
        }
        // 对集合子集操作就是对集合元素的操作;
        System.out.println("list的子集:"+integers);
        System.out.println("list:"+list);
        System.out.println("删除后的:");
        list.remove(0);
        /**
         * 删除之后数组长度变了,输出会报错
         * 原集合修改之后,子集将不能再进行任何操作,操作就异常
         * 但是可以重新获取子集
         */
        System.out.println("list的子集:"+integers);
        System.out.println("list:"+list);
    }
}
