package apiday04;

import java.util.*;

/**
 * @Name: ListSortDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 21:44
 */
public class ListSortDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(r.nextInt(100));
        }
        System.out.println("list原适数据:"+list);
        // 将list集合进行排序
        Collections.sort(list);
        System.out.println("排序后的数据:"+list);
        // 将数据进行逆序排序
        Collections.reverse(list);
        System.out.println("list反转后的数据:"+list);
    }
}
