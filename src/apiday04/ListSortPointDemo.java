package apiday04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Name: ListSortPointDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 22:50
 */
public class ListSortPointDemo {
    /**
     * 排序Point类型
     */
    public static void main(String[] args) {
        List<Point> list = new ArrayList<>();
        list.add(new Point(1,2));
        list.add(new Point(88,5));
        list.add(new Point(3,8));
        list.add(new Point(24,10));
        list.add(new Point(5,9));
        list.add(new Point(25,100));
        System.out.println("list原始数据:"+list);
        Collections.sort(list, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.x-o2.x;
            }
        });
        System.out.println("按照x排序后:"+list);
    }
}
