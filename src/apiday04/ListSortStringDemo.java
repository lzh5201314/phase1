package apiday04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Name: ListSortStringDemo
 * @Author: 西风残叶
 * @Data: 2023/10/13 21:54
 */
/**
 * 字符串集合排序
 */
public class ListSortStringDemo {
    public static void main(String[] args) {
        /**
         * 对汉字排序
         */
        List<String> list1 = new ArrayList<>();
        list1.add("王克兢");
        list1.add("传奇");
        list1.add("苍老师");
        System.out.println("list1原始数据:"+list1);
        // 底层也是按照阿斯克码进行排序的
        System.out.println((int)'王');
        System.out.println((int)'传');
        System.out.println((int)'苍');
        Collections.sort(list1);
        System.out.println("list1排序后的数据"+list1);
        Collections.sort(list1, new Comparator<String>() {
            /*
            o1-o2----------->升序
            o2-o1----------->降序
             */
            @Override
            public int compare(String o1, String o2) {
                return o2.length()-o1.length();
            }
        });
        System.out.println(list1);
        /**
         * 字符串英文排序
         */
        List<String> list = new ArrayList<>();
        list.add("jack");
        list.add("rose");
        list.add("tom");
        list.add("jerry");
        list.add("black");
        list.add("kabe");
        System.out.println("list原数据:"+list);
        // 对英文进行排序时是按照首字母阿斯克码(字母的先后顺序)进行排序,首字母相同则比较第二位的阿斯克码
        Collections.sort(list);
        System.out.println("排序后的数据:"+list);
    }
}
