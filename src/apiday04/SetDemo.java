package apiday04;

import java.util.HashSet;
import java.util.Set;

/**
 * @Name: SetDemo
 * @Author: 西风残叶
 * @Data: 2023/10/14 13:09
 */
public class SetDemo {
    /**
     * Set集合
     */
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("one");
        set.add("tow");
        set.add("three");
        set.add("four");
        set.add("five");
        System.out.println("Set原始数据:"+set);
    }
}
