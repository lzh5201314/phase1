package day02;
/*
 * @ClassName: DataTypeDemo
 * @Description: 数据类型的演示
 * @Data: 2023/9/1 20:09
 * @Author: 西风残叶
 */

public class DataTypeDemo {
    public static void main(String[] args) {
        char c1 = 10;
        System.out.println(c1);
        char c2 = '\'';
        System.out.println(c2);
        char c3 ='\\';
        System.out.println(c3);

        //long:长整型8个字节
        long e = 1000000000*2*10L;
        System.out.println(e);
        // 结果是:200亿
        long f = 1000000000*3*10L;
        System.out.println(f);
        // 结果是1000000000*3溢出的值再

        //int:只能装整数,其他的都不能够装下
        int a = 250;
        int b = 10000000;
        // 直接量超范围会溢出
        int c= 2147483647; // int的最大值
        c=c+1;
        System.out.println(c); // -2147483648
    }
}
