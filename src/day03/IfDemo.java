package day03;
/*
 * @ClassName: IfDemo
 * @Description: 分支结构
 * @Data: 2023/9/20 13:13
 * @Author: 西风残叶
 */

public class IfDemo {
    public static void main(String[] args) {

        // 3.判定年龄是否在18-50之间
        int age = 20;
        if (age > -18 || age <= 50) {
            System.out.println("满足条件");
        }
        System.out.println("继续执行");
        // 2.满500 打八折/
        double price = 300;
        if (price >= 500) {
            price *= 0.8;
        }
        System.out.println("最终金额为"+price);
        // 1.偶数判断
        int num = 2;
        if (num % 2 == 0) {
            System.out.println(num + ":是偶数");
        }
        System.out.println("继续执行.....");
    }
}
