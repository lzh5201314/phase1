package day03;
/*
 * @ClassName: IfelseDemo
 * @Description: TODO
 * @Data: 2023/9/20 13:33
 * @Author: 西风残叶
 */

public class IfelseDemo {
    public static void main(String[] args) {
        // 奇偶数判定
        int num = 9;
        if (num % 2 == 0) {
            System.out.println(num + "为偶数");
        } else {
            System.out.println(num + "为奇数");
        }
    //    打折
        int price = 400;
        if (price>=500){
            price*=0.8;
        }else {
            price*=0.9;
        }
        System.out.println(price);
        System.out.println();
        //判断年龄是否在18-50满足条件成立反之 不成立
        int age = 60 ;
        if (age>=18&&age<=50){
            System.out.println("满足条件");
        }else {
            System.out.println("不满足条件");
        }
    }
}
