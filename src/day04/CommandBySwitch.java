package day04;
/*
 * @ClassName: CommandBySwitch
 * @Description: TODO
 * @Data: 2023/9/20 14:12
 * @Author: 西风残叶
 */

import java.util.Scanner;

public class CommandBySwitch {
    public static void main(String[] args) {
        int command;
        Scanner sc = new Scanner(System.in);
        System.out.println("请选择功能:");
        System.out.println("1.存款");
        System.out.println("2.取款");
        System.out.println("3.查询余额");
        System.out.println("4.退卡");
        command = sc.nextInt();

        switch (command){
            case 1:
                System.out.println("存款操作");
                break;
            case 2:
                System.out.println("取款操作");
                break;
            case 3:
                System.out.println("查询余额操作");
                break;
            case 4:
                System.out.println("退卡操作");
                break;
            default:
                System.out.println("您的操作有误");
        }
    }
}