package day04;
/*
 * @ClassName: Guessing
 * @Description: 猜数字 
 * @Data: 2023/9/20 22:05
 * @Author: 西风残叶
 */

import java.util.Scanner;

public class Guessing {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = 78;
        // 方法一
        while(true){
            System.out.println("请输入那你猜的数(1-100):");
            int guess = sc.nextInt();
            if (guess>num){
                System.out.println("大了");
            }else if(guess<num){
                System.out.println("小了");
            }else {
                System.out.println("恭喜你猜对了");
                break;
            }
        }
    }
}
