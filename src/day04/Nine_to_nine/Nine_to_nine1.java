package day04.Nine_to_nine;
/*
 * @ClassName: Nine_to_nine1
 * @Description: 正常的99表
 * @Data: 2023/9/20 18:39
 * @Author: 西风残叶
 */

public class Nine_to_nine1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i+" * "+j+" = "+i * j +"\t");
            }
            System.out.println();
        }
    }
}
