package day04.Nine_to_nine;
/*
 * @ClassName: Nine_to_nine2
 * @Description: 左上三角99表
 * @Data: 2023/9/20 18:45
 * @Author: 西风残叶
 */

public class Nine_to_nine2 {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 10 - i; j++) {
                System.out.print(i + " * " + j + " = " + i * j+"\t");
            }
            System.out.println();
        }
    }
}
