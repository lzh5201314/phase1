package day04.Nine_to_nine;
/*
 * @ClassName: Nine_to_nine3
 * @Description: 右下99表
 * @Data: 2023/9/20 18:53
 * @Author: 西风残叶
 */

public class Nine_to_nine3 {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            // 设置每行对齐,使得和标准的99表对称,并且不需要每次打印都换行
            for (int k = 1; k < i; k++) {
                System.out.print("\t\t\t");
            }
            for (int j = i; j < 10;j++) {
                System.out.print(i + " * " + j + " = " + (i * j)+"\t");
            }
            System.out.println();
        }
    }
}
