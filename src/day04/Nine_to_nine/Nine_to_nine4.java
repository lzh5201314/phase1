package day04.Nine_to_nine;
/*
 * @ClassName: Nine_to_nine4
 * @Description: TODO
 * @Data: 2023/9/20 21:47
 * @Author: 西风残叶
 */

public class Nine_to_nine4 {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int k = 1; k <= (9-i); k++) {
                System.out.print("\t\t\t");
            }
            for (int j = 10-i; j <10; j++) {
                System.out.print(i + " * " + j + " = " + (i * j)+"\t");
            }
            System.out.println();
        }
    }
}
