package day04.Nine_to_nine;
/*
 * @ClassName: OneFor_Nine_to_nine
 * @Description: TODO
 * @Data: 2023/9/20 21:53
 * @Author: 西风残叶
 */

public class OneFor_Nine_to_nine {
    public static void main(String[] args) {
        for (int i = 1,j = 1 ; i <= 9; i++) {
            System.out.print(i + " * " + j + " = " + (i * j)+"\t");
            // 判断i和j是否相等,主要是为了判断是否是这一行的最后一个
            if (i==j){
                System.out.println();
                i=0;
                j++; // 控制行j为几就是第几行
            }
        }
    }
}





// i的值只能为0,当i=1时机会进行+1操作i的值会变成2