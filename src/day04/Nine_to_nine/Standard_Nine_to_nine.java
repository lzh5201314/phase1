package day04.Nine_to_nine;
/*
 * @ClassName: Standard_Nine_to_nine
 * @Description: 标准的99表
 * @Data: 2023/9/20 18:52
 * @Author: 西风残叶
 */

public class Standard_Nine_to_nine {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++) {
                System.out.print(i + " * " + j + " = " + i * j+"\t");
            }
            System.out.println();
        }
    }
}
