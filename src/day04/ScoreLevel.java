package day04;
/*
 * @ClassName: ScoreLevel
 * @Description: TODO
 * @Data: 2023/9/20 14:08
 * @Author: 西风残叶
 */

import java.util.Scanner;

public class ScoreLevel {
    public static void main(String[] args) {
        double score;
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的成绩:");
        score = sc.nextDouble();
        if (score >= 0 && score <= 100) {
            if (score >= 90) {
                System.out.println("A-优秀");
            } else if (score >= 80) {
                System.out.println("B-良好");
            } else if (score >= 70) {
                System.out.println("C-中等");
            } else if (score >= 60) {
                System.out.println("D-及格");
            } else {
                System.out.println("不及格");
            }
        } else {
            System.out.println("您输入的成绩有误!");
        }
        // 第二种格式:不需要分支嵌套使用
        if (score >= 0 && score <= 100){
            System.out.println("您输入的成绩有误!");
        }else if (score >= 90) {
            System.out.println("A-优秀");
        } else if (score >= 80) {
            System.out.println("B-良好");
        } else if (score >= 70) {
            System.out.println("C-中等");
        } else if (score >= 60) {
            System.out.println("D-及格");
        } else {
            System.out.println("不及格");
        }
    }
}