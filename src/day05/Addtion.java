package day05;
/*
 * @ClassName: Addtion
 * @Description:随机生成10到算术题
 * @Data: 2023/9/22 19:24
 * @Author: 西风残叶
 */

import java.util.Scanner;

public class Addtion {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int score = 0;// 总分
        for (int i = 0; i < 10; i++) {
            int num1 = (int) (Math.random() * 100);// 随机生成两个0-99的数字
            int num2 = (int) (Math.random() * 100);
            System.out.println("("+(i+1)+")  "+num1 + " + " + num2 + " = "); // 对生成的两个数进行加运算
            System.out.println("请输入您的答案:");
            int answer = sc.nextInt();
            if (answer == (num1 + num2)) { // 对输入的答案和两数相加的值进行比较是否相等
                score += 10;
            }
        }
        System.out.println("您的最终得分为:" + score);
    }
}
