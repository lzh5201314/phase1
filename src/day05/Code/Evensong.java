package day05.Code;
/*
 * @ClassName: Evensong
 * @Description: TODO
 * @Data: 2023/9/22 22:11
 * @Author: 西风残叶
 */

public class Evensong {
    /*
    * 如果为while循环的话c为0
    * */
    public static void main(String[] args) {
        int a=0,c=0;
        do {
            --c;
            a--;
        }while (a>0);
        System.out.println(c); //-1
    }
}
