package day05.Code;
/*
 * @ClassName: one
 * @Description: TODO
 * @Data: 2023/9/22 22:04
 * @Author: 西风残叶
 */

public class one {
    /*
    * 循环当中没有break从满足条件开始后面的都会执行
    * */
    public static void main(String[] args) {
        int result = 0;
        int i = 2;
        switch (i){
            case 1:
                result = result+1;
            case 2:
                result = result+i*2; // result =4
            case 3:
                result = result +i*3; // result=10
        }
        System.out.println(result);
    }
}
