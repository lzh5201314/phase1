package day05;
/*
 * @ClassName: MultiTable
 * @Description: 99乘法表
 * @Data: 2023/9/22 19:57
 * @Author: 西风残叶
 */

public class MultiTable {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i+" * "+j+ " = "+i*j+"\t");
            }
            System.out.println();
        }
    }
}
