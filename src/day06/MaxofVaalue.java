package day06;
/*
 * @ClassName: MaxofVaalue
 * @Data: 2023/9/23 14:50
 * @Author: 西风残叶
 */

import java.util.Arrays;

public class MaxofVaalue {
    /*
    * 寻找元素的最大值
    * */
    public static void main(String[] args) {
        int[] arr = new int[10];
        for (int i = 1; i < 10; i++) {
            arr[i] = (int)(Math.random()*100);
        }
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] >max){
                max=arr[i];
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("最大值为:"+max);
    }
}
