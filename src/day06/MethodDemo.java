package day06;
/*
 * @ClassName: MethodDemo
 * @Data: 2023/9/23 20:18
 * @Author: 西风残叶
 */

public class MethodDemo {
    public static void main(String[] args) {
        say();
        System.out.println("over");
    }
    public static void say(){
        System.out.println("大家好!我是Java");
    }
}