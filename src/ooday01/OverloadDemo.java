package ooday01;
/*
 * @ClassName: OverloadDemo
 * @Data: 2023/9/24 13:53
 * @Author: 西风残叶
 */

public class OverloadDemo {
    public static void main(String[] args) {
        Aoo a = new Aoo();
        a.show();
    }
}
class Aoo{
    void show(){
        System.out.println("纱布");
    }
    void show(String name){}

}