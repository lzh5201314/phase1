package ooday01;
/*
 * @ClassName: StudentTest
 * @Data: 2023/9/24 12:42
 * @Author: 西风残叶
 */

public class StudentTest {
    public static void main(String[] args) {
    //    创建一个学生对象
        Student zs = new Student();
    //    访问成员变量
        zs.name="张三";
        zs.age=18;
        zs.address = "陕西西安";
    //    调用方法
        zs.study();
    }
}
