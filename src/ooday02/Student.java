package ooday02;
/*
 * @ClassName: Student
 * @Data: 2023/9/23 22:32
 * @Author: 西风残叶
 */
// 学生类
public class Student {
    // 成员变量
    String name;
    int age;
    String address;
    // 构造器
    Student(String name,int age,String address){
        this.name=name;
        this.age =age;
        this.address = address;
    }
    // 方法
void study(){
    System.out.println(address+"的"+name+"在学习");
}
}